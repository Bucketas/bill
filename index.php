<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

      <!--  <title>Multi-Level Backbone Image Gallery</title> -->
        <link rel="stylesheet" href="gallery.css" type="text/css" media="screen" charset="utf-8" />
        <link rel="stylesheet" href="shadows.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="buttons.css" type="text/css" media="screen" charset="utf-8" />
			


                
                <script id="Before" type="text/x-jquery-tmpl">
                <div class="item drop-shadow round">
                 <div class="item-image before">
                     <a href="#subalbum/${cid}"><img src="${attributes.image}" alt="${attributes.title}" /></a>
                 </div>
                 <div class="item-artist">${attributes.artist}</div>
                    <div class="item-title">${attributes.title}</div>
                    <div class="item-years">${attributes.years}</div>
                </div>
                </script>
				
			
				<script id="ProposalView" type="text/x-jquery-tmpl">
                <div class="item-proposal  drop-shadow round">
                 <div class="item-image subalbum">
                     <a href="#subalbum/${subalbum}/${attributes.pid}"><img src="${attributes.proposal}" alt="${attributes.title}" alt="No images in this folder"/></a> 
                 </div>
                 <div class="item-artist">${attributes.artist}</div>
                    <div class="item-title">${attributes.title}</div>
                    <div class="item-price">$${attributes.price}</div>
                </div>
	
                </script>
				
     
                <script id="SuggestionView" type="text/x-jquery-tmpl">
                <div class="item-detail">
				<div class="item-image fb">
				    <div class="facebook-wrapper"><a href="https://www.facebook.com/sharer/sharer.php?href=http://localhost/json/index.php#subalbum/c0/0"  id="mmm"><img src="fb.png" width="32" height="32"></a></div>
				  </div>
                  <div class="item-suggestion-view  drop-shadow round">
				  <img src="${attributes.large_image}" alt="${attributes.title}" />
				  <img src="${attributes.large_image2}" alt="${attributes.title}" />
						
				  </div>
				  
				
                  
                </div>
                </script>

    </head>
    <body>
        <div id="container">
            

            <div id="main">
                
            </div>
        </div>
        <script src="LAB.min.js" type="text/javascript"></script>

        

        <script type="text/javascript">
		   $LAB
		    .script("jquery-1.4.4.min.js").wait()
		   .script("jquery.tmpl.min.js")
		   .script("underscore-min.js")
		   .script("backbone-min.js")
		   .script("cacheprovider.js").wait()
		   .script("gallery.js"); 	     
        </script>
    </body>
</html>
