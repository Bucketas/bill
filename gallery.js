

var cache = new CacheProvider;


var Proposal = Backbone.Model.extend({
   subalbum: function() { return 'c' + gallery._currentsub; }
});




var ProposalCollection = Backbone.Collection.extend({
    model: Proposal,
	comparator: function(item) {  //seira twn models mesa sto collection
        return item.get('pid');
    }
});


/*To prwto epipedo eikonwn me ta index_view */
var Before = Backbone.View.extend({
    el: $('#main'),
    indexTemplate: $("#Before").template(),

    render: function() {
	
        var sg = this;
        this.el.empty();											//kathe fora pou kaleitai h render xreiazetai na adeiazei to periexomeno tou el
        $.tmpl(sg.indexTemplate, sg.model.toArray()).appendTo(sg.el);
        return this;
    }

});


/*To deutero epipedo eikonwn me ta proposals */
var ProposalView = Backbone.View.extend({
    el: $('#main'),
    indexTemplate: $("#ProposalView").template(),
    initialize: function(options){

    },

    render: function() {
        var sg = this;

            sg.el.empty();
            $.tmpl(sg.indexTemplate, sg.model.toArray()).appendTo(sg.el);
        return this;
    }

});


/*To trito epipedo eikonwn me xehoristo View se kathe proposal */
var SuggestionView = Backbone.View.extend({
    el: $('#main'),
    itemTemplate: $("#SuggestionView").template(),
    
    render: function() {
        var sg = this;   
			sg.el.empty();
            $.tmpl(sg.itemTemplate, sg.model).appendTo(sg.el);
            
        return this;
    }
});




/*O controller */
var Gallery = Backbone.Controller.extend({


    routes: {
        "": "index",
        "subalbum/:id": "hashsub",
        "subalbum/:id/" : "directphoto",
        "subalbum/:id/:num" : "hashphoto"
    },
	initialize: function(options) {
    
        var Gallery = this;
       
     
      
            $.ajax({
					 type: 'GET',
			         url: 'get.php', 
                dataType: 'json',
                stored_data: {},
                success: function(stored_data) {
				    Gallery.all_data = stored_data;
                    Gallery.proposals = new ProposalCollection(stored_data);
                    Gallery.before = new Before({model: Gallery.proposals}); 
					
                    Backbone.history.loadUrl();

                }
            });
            return this;
        
        return this;
    },


   
    index: function() {
        this.before.render();
    },
	

	hashsub:function(id){
		
	   var properindex = id.replace('c','');	
	   this._currentsub = properindex;
	   this._subphotos = cache.get('pc' + properindex) || cache.set('pc' + properindex, new ProposalCollection(this.all_data[properindex].subalbum));
	   this._subalbums = cache.get('sv' + properindex) || cache.set('sv' + properindex, new ProposalView({model: this._subphotos}));
	   this._subalbums.render();

		
	},
	
	directphoto: function(id){

	},


	  hashphoto: function(num, id){
	    this._currentsub = num;
	    
		
		
		
	    num = num.replace('c','');
	    
		if(this._subphotos == undefined){
		   this._subphotos = cache.get('pc' + num) || cache.set('pc' + num, new ProposalCollection(this.all_data[num].subalbum));
		
		 }	
		  
	    this._subphotos.at(id)._view = new SuggestionView({model: this._subphotos.at(id)});
	    this._subphotos.at(id)._view.render();
		
	    sub=this._currentsub;
		url = "https://www.facebook.com/sharer.php?u=http://localhost/json/index.php%23subalbum/"+sub+"/0";
		document.getElementById('mmm').onclick = 
           function () 
		   { window.open(url,'',"height=200,width=200");
		   return false;
		   };
		   
	  }
});


gallery = new Gallery();
Backbone.history.start();


